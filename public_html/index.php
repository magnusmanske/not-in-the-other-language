<?PHP

ini_set('memory_limit','2500M');
error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

require_once ( 'php/common.php' ) ;
require_once ( 'php/wikidata.php' ) ;
require_once ( '/data/project/pagepile/public_html/pagepile.php' ) ;

$projects = array ( 'wiki' => 'wikipedia' ) ;
foreach ( array ( 'wikisource' , 'wikivoyage' , 'wiktionary' ) AS $wiki ) $projects[$wiki] = $wiki ;

$testing = isset($_REQUEST['testing']) ;
$lang = array() ;
$proj = array() ;
$lang[1] = get_request ( 'lang1' , 'de' ) ;
$lang[2] = get_request ( 'lang2' , 'en' ) ;
$proj[1] = get_request ( 'proj1' , 'wiki' ) ;
$proj[2] = get_request ( 'proj2' , 'wiki' ) ;

$start = get_request ( 'start' , 0 ) * 1 ;
$limit = get_request ( 'limit' , 100 ) * 1 ;

$cat = get_request ( 'cat' , '' ) ;
$starts_with = get_request ( 'starts_with' , '' ) ;
$depth = get_request ( 'depth' , '9' ) * 1 ;
//$wdq = get_request ( 'wdq' , '' ) ;
$targets = get_request ( 'targets' , 'wikidata' ) ;
$format = get_request ( 'format' , 'html' ) ;
$pagepile = get_request ( 'pagepile' , '' ) ;
$doit = isset ( $_REQUEST['doit'] ) ;

print get_common_header ( '' , 'Not in the other language' ) ;
?>
<style>
.form-inline label {
	display:inline;
}
</style>
<?PHP
print "<p>This tool looks for Wikidata items that have a page in one language but not in the other.</p>" ;
print "<form method='get' class='form-inline'>" ;
print "<table class='table table-condensed'><tbody>" ;

for ( $i = 1 ; $i <= 2 ; $i++ ) {
	print "<tr><th nowrap>" ;
	print ( $i == 1 ? 'In' : 'Not in' ) ;
	print " language</td><td style='width:100%'><input class='span2' type='text' name='lang$i' id='lang$i' value='" . htmlspecialchars($lang[$i],ENT_QUOTES) . "' /> . <select name='proj$i' id='proj$i' class='form-control custom-select'>" ;
	foreach ( $projects AS $k => $v ) {
		print "<option value='$k'" ;
		if ( $k == $proj[$i] ) print " selected" ;
		print ">$v</option>" ;
	}
	print "</select></td></tr>" ;
}

print "<tr><th>Category tree</th><td><input class='span4' type='text' name='cat' id='cat' value='" . htmlspecialchars($cat,ENT_QUOTES) . "' placeholder='Root category' />, depth <input class='span1' name='depth' type='text' value='$depth' /> (optional)" ;
print "<br/><a href='#' id='featured_link'>Use \"featured article\" category from <span id='featured_lang'></span></a>" ;
print "</td></tr>" ;
print "<tr><th>Page title</th><td>starts with <input class='span4' type='text' name='starts_with' value='".htmlspecialchars($starts_with,ENT_QUOTES)."' /> (optional, case-sensitive)</td></tr>" ;
//print "<tr><th>Wikidata Query</th><td><input class='span4' type='text' name='wdq' id='wdq' value='$wdq' placeholder='Wikidata Query string' /> (optional; see <a href='http://wdq.wmflabs.org/api_documentation.html' target='_blank'>API documentation</a>) <span id='loc'></span></td></tr>" ;

if ( $pagepile_enabeled ) {
	print "<tr><th>PagePile</th><td><input class='span4' type='text' name='pagepile' id='pagepile' value='".htmlspecialchars($pagepile,ENT_QUOTES)."' placeholder='PagePile input ID' /> (optional; check out <a href='/pagepile' target='_blank'>PagePile</a>)</td></tr>" ;
	print "<tr><th>Output</th><td>" ;
	print "Format: <label><input type='radio' name='format' value='html' ".($format=='html'?'checked':'')." /> HTML</label> " ;
	print "<label><input type='radio' name='format' value='pagepile' ".($format=='pagepile'?'checked':'')." /> PagePile</label><br/>" ;
	print "Links to: <label><input type='radio' name='targets' value='wikidata' ".($targets=='wikidata'?'checked':'')." /> Wikidata</label> " ;
	print "<label><input type='radio' name='targets' value='source' ".($targets=='source'?'checked':'')." /> Source Wikipedia</label> " ;
	print "</td></tr>" ;
}

print "<tr><td/><td><input type='submit' name='doit' class='btn btn-primary' value='Do it'/></td></tr>" ;
print "</tbody></table>" ;
print "</form>" ;

?>


<!--<script type="text/javascript" src="./geo.js"></script>-->

<script type="text/javascript">
var sl ;

$(document).ready ( function () {
/*
	if(geo_position_js.init()){
		geo_position_js.getCurrentPosition(function (pos) {
			var lat = pos.coords.latitude ;
			var lon = pos.coords.longitude ;
			var wdq = "around[625,"+lat+","+lon+",15]" ;
			$('#loc').html ( "[<a href='#'>Objects 15 km around your current location</a>]" ) ;
			$('#loc a').click ( function () {
				$('#wdq').val ( wdq ) ;
				return false ;
			} ) ;
		} , function () {
			// console.log("Cannot determine location");
		});
	}
*/	
	function updateFAlink () {
		var l = $('#lang1').val() ;
		var p = $('#proj1').val() ;
		$('#featured_lang').text ( l+p ) ;
	}
	
	$('#proj1').change ( updateFAlink ) ;
	$('#lang1').keyup ( updateFAlink ) ;
	$('#featured_link').click ( function () {
		var wiki = $('#lang1').val() + $('#proj1').val() ;
		if ( typeof sl[wiki] == 'undefined' ) {
			alert ( wiki + " has no Featured Articles category!" ) ;
		} else {
			$('#cat').val ( sl[wiki].title.replace(/^[^:]+:/,'') ) ;
		}
		return false ;
	} ) ;

	var url = "https://www.wikidata.org/w/api.php?action=wbgetentities&ids=Q4387444&format=json&callback=?" ;
	$.getJSON ( url , function ( d ) {
		sl = d.entities.Q4387444.sitelinks ;
		updateFAlink() ;
	} ) ;
	
} ) ;

</script>

<?PHP

function get_content_lang($lang)
{
	$content_lang_map = array(
		'als' => 'gsw',
		'bat-smg' => 'sgs',
		'be-x-old' => 'be-tarask',
		'bh' => 'bho',
		'crh' => 'chr-latn',
		'fiu-vro' => 'vro',
		'no' => 'nb',
		'roa-rup' => 'rup',
		'simple' => 'en',
		'zh-classical' => 'lzh',
		'zh-min-nan' => 'nan',
		'zh-yue' => 'yue',
	);
	return isset($content_lang_map[$lang]) ? $content_lang_map[$lang] : $lang;
}

if ( $doit ) {

	print "<hr/>" ;
	$k1 = get_db_safe ( $lang[1].$proj[1] ) ;
	$k2 = get_db_safe ( $lang[2].$proj[2] ) ;
	
	$sql = "SELECT i1.ips_item_id from wb_items_per_site i1" ;
	$sql .= " LEFT OUTER JOIN wb_items_per_site i2 ON i1.ips_item_id=i2.ips_item_id AND i2.ips_site_id='$k2' WHERE i1.ips_site_id='$k1' AND i2.ips_site_id IS NULL" ;
	if ( $starts_with != '' ) $sql .= " AND i1.ips_site_page >= '" . get_db_safe ( $starts_with ) . "'" ;
	if ( $cat != '' ) {
//print "<pre>{$lang[1]}.{$projects[$proj[1]]}</pre>" ;
		$db2 = openDB ( $lang[1] , $projects[$proj[1]] ) ;
		$pages = getPagesInCategory ( $db2 , ucfirst(trim($cat)) , $depth ) ;
//print "<pre>" ; print_r ( $pages ) ; print "</pre>" ;
		$p2 = array() ;
		foreach ( $pages AS $k => $v ) $p2[] = str_replace ( '_' , ' ' , get_db_safe ( $v ) ) ;
		$sql .= " AND i1.ips_site_page IN ('" . join ( "','" , $p2 ) . "')" ;
	}
	if ( $pagepile != '' ) {
		$pp = new PagePile ( $pagepile ) ;
		$pp = $pp->getAsWikidata() ;
		$pages = $pp->getArray() ;
		foreach ( $pages AS $k => $v ) {
			$pages[$k] = preg_replace ( '/\D/' , '' , $v->page ) ;
		}
		$sql .= " AND i1.ips_item_id IN (" . implode(',',$pages) . ")" ;
#		print "<pre>$sql</pre>" ;
	}
/*
	if ( $wdq ) {
		$url = "$wdq_internal_url?q=" . urlencode($wdq) ;
		$j = json_decode ( file_get_contents ( $url ) ) ;
		$sql .= " AND i1.ips_item_id IN (" . implode(',',$j->items) . ")" ;
	}
*/

	$generate_pagepile = $format == 'pagepile' ;
	$pp = '' ;
	if ( $generate_pagepile ) {
		$pp = new PagePile ;
		$pp->createNewPile ( 'wikidata' , 'wikidata' ) ;
	} else {
		$sql .= " order by i1.ips_site_page limit " . get_db_safe ( $limit ) . " OFFSET " . get_db_safe ( $start ) ;
	}
	
	$items = array() ;
	$db = openDB ( 'wikidata' , 'wikimedia' ) ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()){
		if ( $generate_pagepile ) $pp->addPage ( "Q".$o->ips_item_id , 0 ) ;
		else $items[$o->ips_item_id] = "Q".$o->ips_item_id ;
	}
	
	if ( $generate_pagepile ) {
		if ( $targets == 'source' ) $pp->fromWikidata ( $k1 ) ;
		$pp->printAndEnd() ;
	}

	if ( $targets == 'source' ) {
		$sql = "select * from wb_items_per_site WHERE ips_item_id IN ('" . join ( "','" , array_keys($items) ) . "') AND ips_site_id='$k1'" ;
		if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
		while($o = $result->fetch_object()){
			$items[$o->ips_item_id] = $o->ips_site_page ;
		}
	} else {
		$wil = new WikidataItemList ;
		$label_language = get_content_lang($lang[1]) ;
		$res = $wil->getItemLabels(array_values($items),$label_language) ;
		foreach ( $res AS $q => $label ) $items[preg_replace('/\D/','',$q)] = $label ;
	}

	print "<h3>Results " . ($start+1) . "&ndash;" . ($start+$limit) . "</h3>" ;

	$list = "<ol>" ;
	$cnt = 0 ;
	$server1 = getWebserverForWiki ( $k1 ) ;
	foreach ( $items AS $k => $v ) {
		$list .= "<li" ;
		if ( $cnt == 0 ) $list .= " value='" . ($start+1) . "'" ;

		if ( $targets == 'source' ) {
			$list .= "><a target='_blank' href='//$server1/wiki/".myurlencode($v)."'>$v</a></li>" ;
		} else {
			$list .= "><a target='_blank' href='//www.wikidata.org/wiki/Q$k'>$v</a></li>" ;
		}
		$cnt++ ;
	}
	$list .= "</ol>" ;

	$last_next = "<div style='margin-bottom:10px'>" ;
	if ( $start > 0 ) $last_next .= "<a href='?lang1={$lang[1]}&proj1={$proj[1]}&lang2={$lang[2]}&proj2={$proj[2]}&cat=".htmlspecialchars($cat,ENT_QUOTES)."&depth=$depth&limit=$limit&starts_with=$starts_with&start=" . ($start-$limit) . "&targets=$targets&doit=1'>prev</a>" ;
	else if ( $cnt == $limit || $start > 0 ) $last_next .= "prev" ;
	if ( $cnt == $limit ) $last_next .= " | <a href='?lang1={$lang[1]}&proj1={$proj[1]}&lang2={$lang[2]}&proj2={$proj[2]}&cat=".htmlspecialchars($cat,ENT_QUOTES)."&depth=$depth&limit=$limit&starts_with=$starts_with&start=" . ($start+$limit) . "&targets=$targets&doit=1'>next</a>" ;
	$last_next .= "</div>" ;

	print $last_next . $list . $last_next ;
}

print get_common_footer() ;

?>